//
//  FavoriteButtonView.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import SwiftUI

struct FavoriteButtonView: View {
    @ObservedObject var favoriteVM = FavoriteViewModel()
    
    let movieVM: MovieViewModel
    let defaultConditon: Bool = false
    
    init(movieVM: MovieViewModel) {
        self.movieVM = movieVM
        favoriteVM.setDefaultCondition(movieVM.isFavorited())
    }
    
    private func buttonColor(_ isActive: Bool) -> Color {
        if isActive {
            return .red
        }
        return .gray
    }
    
    var body: some View {
        Button("Favoritkan +") {
            self.favoriteVM.didTapFavorite(self.movieVM.data)
        }.background(buttonColor(favoriteVM.isFavorited))
    }
}
