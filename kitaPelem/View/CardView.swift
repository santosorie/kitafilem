//
//  CardView.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import SwiftUI

struct CardView: View {
    
    private let movie: MovieViewModel
    init(loadWith movie: MovieViewModel) {
        self.movie = movie
    }
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                PictureView(loadWith: movie.posterUrl).frame(width: CGFloat(120), height: CGFloat(120), alignment: .leading).padding(.horizontal, 16)
                NavigationLink("more...", destination: DetailPageView(loadWith: movie)).padding(.leading, 16).foregroundColor(Color.white)
            }
            VStack(alignment: .leading) {
                Text("\(movie.title)").lineLimit(1).font(.system(.headline))
                Text("\(movie.releaseDate)").lineLimit(1).font(.system(.caption))
                Divider()
                Text("\(movie.overview)").lineLimit(5).font(.system(.footnote))
            }.padding(.trailing, 16)
        }.frame(width: UIScreen.main.bounds.width, height: CGFloat(180), alignment: .leading).background(Color.blue)
    }
}
