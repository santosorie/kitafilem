//
//  PictureView.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import SwiftUI

struct PictureView: View {
    
    init(loadWith url: String) {
        imageLoader.loadImage(with: url)
    }
    
    @ObservedObject var imageLoader: ImageLoader = ImageLoader()
    
    var body: some View {
        Image(uiImage: imageLoader.image)
            .resizable()
            .background(Color.yellow)
    }
}
