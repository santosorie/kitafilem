//
//  ContentView.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 09/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var listViewModel = MovieListViewModel()
    @ObservedObject var filterViewModel = FilterViewModel()
    
    private func buttonColor(_ isActive: Bool) -> Color {
        if isActive {
            return .gray
        }
        return .white
    }
    
    private func handleTapFilter(_ filter: FilterViewModel.Categories) {
        switch filter {
        case .home:
            listViewModel.didTapHome()
        case .favorite:
            listViewModel.didTapFavorite()
        case .populer:
            listViewModel.didTapPopuler()
        case .topRated:
            listViewModel.didTapRating()
        case .nowPlaying:
            listViewModel.didTapNowPlaying()
        }
    }
    
    var body: some View {
        NavigationView {
            VStack() {
                ScrollView(.horizontal, showsIndicators: true) {
                    HStack(alignment: .center, spacing: 32) {
                        Spacer()
                        ForEach(filterViewModel.categories) { category in
                            Button(category.rawValue) {
                                self.filterViewModel.isActive = category
                                self.handleTapFilter(category)
                            }.background(self.buttonColor(self.filterViewModel.isActive == category))
                        }
                        Spacer()
                    }
                }.frame(height: 60).background(Color.black).padding(.vertical, 16)
                showView(by: listViewModel.viewState)
                Spacer()
            }.navigationBarTitle("kita filem")
        }
    }
    
    private func showView(by state: MovieListViewModel.ViewState) -> AnyView {
        switch state {
        case .fetching:
            return AnyView(Text("fetching"))
        case .success(let movies):
            return AnyView(ScrollView {
                VStack {
                    ForEach(movies) { movie in
                        AnyView(self.movieCard(movie))
                    }
                }
            })
        case .empty:
            return AnyView(Text("kosong"))
        case .error:
            return AnyView(Text("error"))
        }
    }
    
    private func movieCard(_ movie: MovieViewModel) -> some View {
        CardView(loadWith: movie)
    }
}
