//
//  DetailPageView.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import SwiftUI

struct DetailPageView: View {
    private let movieVM: MovieViewModel
    init(loadWith movieVM: MovieViewModel) {
        self.movieVM = movieVM
    }
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                PictureView(loadWith: movieVM.posterUrl).padding(.horizontal, 16).frame(height: 400)
                FavoriteButtonView(movieVM: movieVM).padding(16)
                Text("\(movieVM.title)").lineLimit(1).font(.system(.title)).padding(.horizontal, 16)
                Text("\(movieVM.releaseDate)").lineLimit(1).font(.system(.callout)).padding(.horizontal, 16)
                Divider()
                Text("\(movieVM.overview)").font(.system(.body)).padding(.horizontal, 16)
                Divider()
                ReviewsView(movieId: movieVM.id).padding(.horizontal, 16)
                Spacer()
            }.frame(width: UIScreen.main.bounds.width, alignment: .leading).background(Color.blue)
        }
    }
}

struct ReviewsView: View {
    @ObservedObject var reviewListVM = ReviewListViewModel()
    
    init(movieId: Int64) {
        reviewListVM.getReviewList(movieId: movieId)
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            if !reviewListVM.reviews.isEmpty {
                Text("Review").font(.system(.title))
            }
            ForEach(reviewListVM.reviews) { reviewVM in
                Divider().background(Color.black)
                Text(reviewVM.author).font(.system(.callout))
                Text(reviewVM.content).font(.system(.caption)).lineLimit(20)
            }
        }
        
    }
}
