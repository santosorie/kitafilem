//
//  CacheManager.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

class CacheManager {
    static let shared = CacheManager()
    let defaults = UserDefaults.standard
    
    let favoriteListKey = "favorite_list"
    
    var favoriteMovies: [Movie]? {
        guard let data = defaults.object(forKey: favoriteListKey) as? Data else {
            return nil
        }
        
        do {
            return try JSONDecoder().decode([Movie].self, from: data)
        } catch {
            return nil
        }
    }
    
    func saveMovie(_ movie: Movie) {
        if let favoriteList = favoriteMovies {
            var newList = [movie]
            newList = favoriteList + newList
            saveData(newList)
        } else {
            saveData([movie])
        }
    }
    
    func removeMovie(_ movie: Movie) {
        if let favoriteList = favoriteMovies {
            var newList = favoriteList
            newList.removeAll { $0.id == movie.id }
            saveData(newList)
        }
    }
    
    private func saveData(_ movies: [Movie]) {
        do {
            let data = try JSONEncoder().encode(movies)
            defaults.set(data, forKey: favoriteListKey)
        } catch (let error) {
            print("error \(error)")
        }
    }
}
