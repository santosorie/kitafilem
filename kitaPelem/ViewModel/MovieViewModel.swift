//
//  MovieViewModel.swift
//  kitaPelemTests
//
//  Created by Harrie Santoso on 10/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

struct MovieViewModel: Identifiable {
    let id: Int64
    private let movie: Movie
    private let cacheManager: CacheManager
    
    init(movie: Movie,
         cacheManager: CacheManager = .shared) {
        self.movie = movie
        self.id = movie.id
        self.cacheManager = cacheManager
    }
    
    var data: Movie {
        return movie
    }
    
    var title: String {
        return movie.title
    }
    
    var releaseDate: String {
        return movie.releaseDate
    }
    
    var overview: String {
        return movie.overview
    }
    
    var posterUrl: String {
        return movie.posterPath
    }
    
    func isFavorited() -> Bool {
        guard let favoriteList = cacheManager.favoriteMovies else {
            return false
        }
        
        return favoriteList.contains { $0.id == movie.id }
    }
}
