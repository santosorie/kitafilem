//
//  MovieListViewModel.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

class MovieListViewModel: ObservableObject {
    let fetcher: MovieFetcherInterface
    let cacheManager: CacheManager
    
    enum ViewState: Equatable {
        static func == (lhs: MovieListViewModel.ViewState, rhs: MovieListViewModel.ViewState) -> Bool {
            switch (lhs, rhs) {
            case (let .success(lMoviesVM), let .success(rMoviesVM)):
                return lMoviesVM.first?.id == rMoviesVM.first?.id
            case (.fetching, .fetching):
                return true
            case (.error, .error):
                return true
            case (.empty, .empty):
                return true
            default:
                return false
            }
        }
        
        case fetching
        case success([MovieViewModel])
        case error
        case empty
    }
    
    init(fetcher: MovieFetcherInterface = MovieFetcher(),
         cacheManager: CacheManager = .shared) {
        self.fetcher = fetcher
        self.cacheManager = cacheManager
        fetchMovies()
    }
    
    @Published var viewState: ViewState = .fetching
    
    func didTapHome() {
        viewState = .fetching
        fetchMovies()
    }
    
    func didTapFavorite() {
        getFavoritesFromCache()
    }
    
    func didTapPopuler() {
        viewState = .fetching
        fetchPopular()
    }
    
    func didTapRating() {
        viewState = .fetching
        fetchTopRated()
    }
    
    func didTapNowPlaying() {
        viewState = .fetching
        fetchNowPlaying()
    }
    
    private func getFavoritesFromCache() {
        if let favoriteMovies: [Movie] = cacheManager.favoriteMovies {
            let movies: [MovieViewModel] = favoriteMovies.map { MovieViewModel(movie: $0) }
            viewState = .success(movies)
        } else {
            viewState = .empty
        }
    }
    
    private func fetchMovies() {
        fetcher.getMovieList { [unowned self] response in
            self.handleResponse(response)
        }
    }
    
    private func fetchPopular() {
        fetcher.getPopular { [unowned self] response in
            self.handleResponse(response)
        }
    }
    
    private func fetchTopRated() {
        fetcher.getTopRated { [unowned self] response in
            self.handleResponse(response)
        }
    }
    
    private func fetchNowPlaying() {
        fetcher.getNowPlaying { [unowned self] response in
            self.handleResponse(response)
        }
    }
    
    private func handleResponse(_ response: MovieResponse) {
        switch response {
        case .success(let movies):
            let moviesVM = movies.map { MovieViewModel(movie: $0)}
            self.viewState = .success(moviesVM)
        case .error:
            self.viewState = .error
        }
    }
}
