//
//  FilterViewModel.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 10/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

class FilterViewModel: ObservableObject {
    
    enum Categories: String, Identifiable {
        var id: String {
            return self.rawValue
        }
        
        case home = "Home"
        case favorite = "Favorit"
        case populer = "Populer"
        case topRated = "Top Rating"
        case nowPlaying = "Sekarang Tayang"
    }
    
    @Published var isActive: Categories = .home
    var categories: [Categories] = [.home, .favorite, .populer, .topRated, .nowPlaying]
}
