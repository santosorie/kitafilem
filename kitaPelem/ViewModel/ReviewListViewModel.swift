//
//  ReviewListViewModel.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

struct ReviewViewModel: Identifiable {
    let id: String
    let review: Review
    
    init(review: Review) {
        self.review = review
        id = review.id
    }
    
    var author: String {
        return review.author
    }
    
    var content: String {
        return review.content
    }
}

class ReviewListViewModel: ObservableObject {
    @Published var reviews: [ReviewViewModel] = []
    
    let reviewFetcher: ReviewFetcherInterface
    
    init(reviewFetcher: ReviewFetcherInterface = ReviewFetcher()) {
        self.reviewFetcher = reviewFetcher
    }
    
    func getReviewList(movieId: Int64) {
        reviewFetcher.getReviewList(movieId: movieId) { response in
            switch response {
            case .success(let reviews):
                self.reviews = reviews.map { ReviewViewModel(review: $0) }
            case .error:
                //nothing happen
                break
            }
        }
    }
}
