//
//  FavoriteViewModel.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

class FavoriteViewModel: ObservableObject {
    @Published var isFavorited: Bool = false
    private let cacheManager: CacheManager
    
    init(cacheManager: CacheManager = .shared) {
        self.cacheManager = cacheManager
    }
    
    func setDefaultCondition(_ condition: Bool) {
        isFavorited = condition
    }
    
    func didTapFavorite(_ movie: Movie) {
        isFavorited = !isFavorited
        
        if isFavorited {
            cacheManager.saveMovie(movie)
        } else {
            cacheManager.removeMovie(movie)
        }
    }
}
