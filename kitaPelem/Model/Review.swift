//
//  Review.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

struct Review: Codable {
    var id: String
    var author: String
    var content: String
}

struct ReviewResult: Codable {
    var results: [Review]
}
