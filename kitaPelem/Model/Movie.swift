//
//  Movie.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 10/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

struct Movie: Codable {
    var id: Int64
    var title: String
    var releaseDate: String
    var overview: String
    var posterPath: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case releaseDate = "release_date"
        case overview
        case posterPath = "poster_path"
    }
}

struct MovieResults: Decodable {
    var results: [Movie]
}
