//
//  ImageLoader.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 10/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation
import Alamofire

class ImageLoader: ObservableObject {
    
    @Published var image: UIImage = UIImage()
    
    func loadImage(with url: String) {
        let baseUrl = "https://image.tmdb.org/t/p/w185/"
        retrieveImage(url: baseUrl + url) { [weak self] data in
            DispatchQueue.main.async {
                if let image = UIImage(data: data) {
                    self?.image = image
                }
            }
        }
    }
    
    private func retrieveImage(url: String, completion: @escaping ((Data) -> Void)) {
        AF.request(url, method: .get).responseData(queue: .global(qos: .background)) { response in
            switch response.result {
            case .success(let data):
                completion(data)
            case .failure(let error):
                print("error: \(error)")
            }
        }
    }
}
