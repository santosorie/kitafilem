//
//  MovieFetcher.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 10/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation
import Alamofire

enum MovieResponse {
    case success([Movie])
    case error
}

typealias responseClosure = (MovieResponse) -> Void

protocol MovieFetcherInterface {
    func getMovieList(fRespone: @escaping responseClosure)
    func getPopular(fRespone: @escaping responseClosure)
    func getTopRated(fRespone: @escaping responseClosure)
    func getNowPlaying(fRespone: @escaping responseClosure)
}

protocol ApiInfo {
    var apiKey: String { get }
    var apiKeyValue: String { get }
    var baseUrl: String { get }
}

extension ApiInfo {
    var apiKey: String {
        return "api_key"
    }
    
    var apiKeyValue: String {
        return "d0e5b170ee60aa4ab5add891020ebf83"
    }
    
    var baseUrl: String {
        return "https://api.themoviedb.org/3/"
    }
}

class MovieFetcher: MovieFetcherInterface, ApiInfo {
    func getMovieList(fRespone: @escaping responseClosure) {
        let getMovieUrl: String = baseUrl + "discover/movie"
        let params: Parameters = [apiKey: apiKeyValue]
        AF.request(getMovieUrl, parameters: params).responseJSON { response in
            switch response.result {
            case .success:
                do {
                    if let data = response.data {
                        let movieData = try JSONDecoder().decode(MovieResults.self, from: data)
                        fRespone(MovieResponse.success(movieData.results))
                    } else {
                        fRespone(MovieResponse.error)
                    }
                } catch (let error) {
                    print("error: \(error)")
                    fRespone(MovieResponse.error)
                }
            case .failure:
                fRespone(MovieResponse.error)
            }
        }
    }
    
    func getPopular(fRespone: @escaping responseClosure) {
        let getPopularUrl: String = baseUrl + "movie/popular"
        let params: Parameters = [apiKey: apiKeyValue]
        AF.request(getPopularUrl, parameters: params).responseJSON { response in
            switch response.result {
            case .success:
                do {
                    if let data = response.data {
                        let movieData = try JSONDecoder().decode(MovieResults.self, from: data)
                        fRespone(MovieResponse.success(movieData.results))
                    } else {
                        fRespone(MovieResponse.error)
                    }
                } catch (let error) {
                    print("error: \(error)")
                    fRespone(MovieResponse.error)
                }
            case .failure:
                fRespone(MovieResponse.error)
            }
        }
    }
    
    func getTopRated(fRespone: @escaping responseClosure) {
        let getTopUrl: String = baseUrl + "movie/top_rated"
        let params: Parameters = [apiKey: apiKeyValue]
        AF.request(getTopUrl, parameters: params).responseJSON { response in
            switch response.result {
            case .success:
                do {
                    if let data = response.data {
                        let movieData = try JSONDecoder().decode(MovieResults.self, from: data)
                        fRespone(MovieResponse.success(movieData.results))
                    } else {
                        fRespone(MovieResponse.error)
                    }
                } catch (let error) {
                    print("error: \(error)")
                    fRespone(MovieResponse.error)
                }
            case .failure:
                fRespone(MovieResponse.error)
            }
        }
    }
    
    func getNowPlaying(fRespone: @escaping responseClosure) {
        let getNowPlayingUrl: String = baseUrl + "movie/now_playing"
        let params: Parameters = [apiKey: apiKeyValue]
        AF.request(getNowPlayingUrl, parameters: params).responseJSON { response in
            switch response.result {
            case .success:
                do {
                    if let data = response.data {
                        let movieData = try JSONDecoder().decode(MovieResults.self, from: data)
                        fRespone(MovieResponse.success(movieData.results))
                    } else {
                        fRespone(MovieResponse.error)
                    }
                } catch (let error) {
                    print("error: \(error)")
                    fRespone(MovieResponse.error)
                }
            case .failure:
                fRespone(MovieResponse.error)
            }
        }
    }
}
