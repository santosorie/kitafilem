//
//  ReviewFetcher.swift
//  kitaPelem
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation
import Alamofire

enum ReviewResponse {
    case success([Review])
    case error
}

protocol ReviewFetcherInterface {
    func getReviewList(movieId: Int64, fResponse: @escaping (ReviewResponse) -> Void)
}

class ReviewFetcher: ReviewFetcherInterface, ApiInfo {
    func getReviewList(movieId: Int64, fResponse: @escaping (ReviewResponse) -> Void) {
        let getReviewsUrl: String = baseUrl + "movie/\(movieId)/reviews"
        let params: Parameters = [apiKey: apiKeyValue]
        AF.request(getReviewsUrl, parameters: params).responseJSON { response in
            print(response)
            switch response.result {
            case .success:
                do {
                    if let data = response.data {
                        let reviewData = try JSONDecoder().decode(ReviewResult.self, from: data)
                        fResponse(ReviewResponse.success(reviewData.results))
                    } else {
                        fResponse(ReviewResponse.error)
                    }
                } catch (let error) {
                    print("error: \(error)")
                    fResponse(ReviewResponse.error)
                }
            case .failure:
                fResponse(ReviewResponse.error)
            }
        }
    }
}
