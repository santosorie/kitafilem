//
//  MovieListViewModelTests.swift
//  kitaPelemTests
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import XCTest
@testable import kitaPelem

class MovieListViewModelTests: XCTestCase {
    var movieListVM: MovieListViewModel!
    var mockedMovieFetcher: MockedMovieFetcher!
    var mockedCacheManager: MockedCacheManager!
    let randomizer = Randomizer()
    
    override func setUp() {
        mockedMovieFetcher = MockedMovieFetcher()
        mockedCacheManager = MockedCacheManager()
        movieListVM = MovieListViewModel(fetcher: mockedMovieFetcher,
                                         cacheManager: mockedCacheManager)
    }
    
    override func tearDown() {
        mockedMovieFetcher = nil
        movieListVM = nil
    }
    
    func testDidTapHome() {
        //given
        let mockedMovie = randomizer.getMovie()
        mockedMovieFetcher.stimulateSuccess = true
        mockedMovieFetcher.mockedMovieResult = [mockedMovie]
        //when
        movieListVM.didTapHome()
        //then
        XCTAssertEqual(movieListVM.viewState, .success([MovieViewModel(movie: mockedMovie)]))
        XCTAssertTrue(mockedMovieFetcher.isGetMovieCalled)
    }
    
    func testDidTapHomeFail() {
        //given
        let mockedMovie = randomizer.getMovie()
        mockedMovieFetcher.stimulateSuccess = false
        mockedMovieFetcher.mockedMovieResult = [mockedMovie]
        //when
        movieListVM.didTapHome()
        //then
        XCTAssertEqual(movieListVM.viewState, .error)
        XCTAssertTrue(mockedMovieFetcher.isGetMovieCalled)
    }
    
    func testDidTapPopular() {
        //given
        let mockedMovie = randomizer.getMovie()
        mockedMovieFetcher.stimulateSuccess = true
        mockedMovieFetcher.mockedMovieResult = [mockedMovie]
        //when
        movieListVM.didTapPopuler()
        //then
        XCTAssertEqual(movieListVM.viewState, .success([MovieViewModel(movie: mockedMovie)]))
        XCTAssertTrue(mockedMovieFetcher.isGetPopularCalled)
    }
    
    func testDidTapPopularFail() {
        //given
        let mockedMovie = randomizer.getMovie()
        mockedMovieFetcher.stimulateSuccess = false
        mockedMovieFetcher.mockedMovieResult = [mockedMovie]
        //when
        movieListVM.didTapPopuler()
        //then
        XCTAssertEqual(movieListVM.viewState, .error)
        XCTAssertTrue(mockedMovieFetcher.isGetPopularCalled)
    }
    
    func testDidTapTopRated() {
        //given
        let mockedMovie = randomizer.getMovie()
        mockedMovieFetcher.stimulateSuccess = true
        mockedMovieFetcher.mockedMovieResult = [mockedMovie]
        //when
        movieListVM.didTapRating()
        //then
        XCTAssertEqual(movieListVM.viewState, .success([MovieViewModel(movie: mockedMovie)]))
        XCTAssertTrue(mockedMovieFetcher.isGetTopRatedCalled)
    }
    
    func testDidTapTopRatedFail() {
        //given
        let mockedMovie = randomizer.getMovie()
        mockedMovieFetcher.stimulateSuccess = false
        mockedMovieFetcher.mockedMovieResult = [mockedMovie]
        //when
        movieListVM.didTapRating()
        //then
        XCTAssertEqual(movieListVM.viewState, .error)
        XCTAssertTrue(mockedMovieFetcher.isGetTopRatedCalled)
    }
    
    func testDidTapNowPlaying() {
        //given
        let mockedMovie = randomizer.getMovie()
        mockedMovieFetcher.stimulateSuccess = true
        mockedMovieFetcher.mockedMovieResult = [mockedMovie]
        //when
        movieListVM.didTapNowPlaying()
        //then
        XCTAssertEqual(movieListVM.viewState, .success([MovieViewModel(movie: mockedMovie)]))
        XCTAssertTrue(mockedMovieFetcher.isGetNowPlayingCalled)
    }
    
    func testDidTapNowPlayingFail() {
        //given
        let mockedMovie = randomizer.getMovie()
        mockedMovieFetcher.stimulateSuccess = false
        mockedMovieFetcher.mockedMovieResult = [mockedMovie]
        //when
        movieListVM.didTapNowPlaying()
        //then
        XCTAssertEqual(movieListVM.viewState, .error)
        XCTAssertTrue(mockedMovieFetcher.isGetNowPlayingCalled)
    }
    
    func testDidTapFavorite() {
        //given
        let mockedMovie = randomizer.getMovie()
        mockedCacheManager.mockedFavoriteMovies = [mockedMovie]
        //when
        movieListVM.didTapFavorite()
        //then
        XCTAssertEqual(movieListVM.viewState, .success([MovieViewModel(movie: mockedMovie)]))
    }
    
    func testDidTapFavoriteNil() {
        //given
        mockedCacheManager.mockedFavoriteMovies = nil
        //when
        movieListVM.didTapFavorite()
        //then
        XCTAssertEqual(movieListVM.viewState, .empty)
    }
}
