//
//  FavoriteViewModelTests.swift
//  kitaPelemTests
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import XCTest
@testable import kitaPelem

class FavoriteViewModelTests: XCTestCase {

    var favoriteVM: FavoriteViewModel!
    var mockedCacheManager: MockedCacheManager!
    let randomizer = Randomizer()
    
    override func setUp() {
        mockedCacheManager = MockedCacheManager()
        favoriteVM = FavoriteViewModel(cacheManager: mockedCacheManager)
    }

    func testTurnOnFavorite() {
        //given
        let movie = randomizer.getMovie()
        favoriteVM.isFavorited = false
        //when
        favoriteVM.didTapFavorite(movie)
        //then
        XCTAssertEqual(mockedCacheManager.savedMovie.id, movie.id)
        XCTAssertTrue(favoriteVM.isFavorited)
    }
    
    func testTurnOffFavorite() {
        //given
        let movie = randomizer.getMovie()
        favoriteVM.isFavorited = true
        //when
        favoriteVM.didTapFavorite(movie)
        //then
        XCTAssertEqual(mockedCacheManager.removedMovie.id, movie.id)
        XCTAssertFalse(favoriteVM.isFavorited)
    }
    
    func testSetDefaultCondition() {
        //given
        let condition = randomizer.getBool()
        //when
        favoriteVM.setDefaultCondition(condition)
        //then
        XCTAssertEqual(favoriteVM.isFavorited, condition)
    }
}
