//
//  ReviewListViewModelTests.swift
//  kitaPelemTests
//
//  Created by Harrie Santoso on 13/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import XCTest
@testable import kitaPelem

class ReviewListViewModelTests: XCTestCase {
    var reviewListVM: ReviewListViewModel!
    var mockedReviewFetcher: MockedReviewFetcher!
    let randomizer = Randomizer()
    
    override func setUp() {
        mockedReviewFetcher = MockedReviewFetcher()
        reviewListVM = ReviewListViewModel(reviewFetcher: mockedReviewFetcher)
    }
    
    func testGetReviewList() {
        //given
        let review = randomizer.getReview()
        mockedReviewFetcher.mockedReviewResult = [review]
        mockedReviewFetcher.stimulateSuccess = true
        //when
        reviewListVM.getReviewList(movieId: 0)
        //then
        XCTAssertEqual(reviewListVM.reviews.first?.id, review.id)
    }
    
    func testGetReviewListFail() {
        //given
        let review = randomizer.getReview()
        mockedReviewFetcher.mockedReviewResult = [review]
        mockedReviewFetcher.stimulateSuccess = false
        //when
        reviewListVM.getReviewList(movieId: 0)
        //then
        XCTAssertEqual(reviewListVM.reviews.count, 0)
    }
}
