//
//  MovieViewModelTests.swift
//  kitaPelemTests
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import XCTest
@testable import kitaPelem
    
final class MovieViewModelTests: XCTestCase {
    
    var movieViewModel: MovieViewModel!
    var mockedCacheManager: MockedCacheManager!
    let randomizer = Randomizer()
    
    override func setUp() {
        movieViewModel = MovieViewModel(movie: randomizer.getMovie())
        mockedCacheManager = MockedCacheManager()
    }
    
    func testValues() {
        //given
        let movie = randomizer.getMovie()
        //when
        movieViewModel = MovieViewModel(movie: movie)
        //then
        XCTAssertEqual(movieViewModel.data.id, movie.id)
        XCTAssertEqual(movieViewModel.title, movie.title)
        XCTAssertEqual(movieViewModel.releaseDate, movie.releaseDate)
        XCTAssertEqual(movieViewModel.releaseDate, movie.releaseDate)
        XCTAssertEqual(movieViewModel.posterUrl, movie.posterPath)
    }
    
    func testFavoriteMoviesNil() {
        //given
        mockedCacheManager.mockedFavoriteMovies = nil
        //when
        movieViewModel = MovieViewModel(movie: randomizer.getMovie(),
                                        cacheManager: mockedCacheManager)
        //then
        XCTAssertFalse(movieViewModel.isFavorited())
    }
    
    func testNotFavoriteMovies() {
        //given
        let favoritedMovies = randomizer.getMovie()
        let testedMovies = randomizer.getMovie(with: favoritedMovies.id + 1)
        mockedCacheManager.mockedFavoriteMovies = [favoritedMovies]
        //when
        movieViewModel = MovieViewModel(movie: testedMovies,
                                        cacheManager: mockedCacheManager)
        //then
        XCTAssertFalse(movieViewModel.isFavorited())
    }
    
    func testHaveFavoriteMovies() {
        //given
        let favoritedMovies = randomizer.getMovie()
        mockedCacheManager.mockedFavoriteMovies = [favoritedMovies]
        //when
        movieViewModel = MovieViewModel(movie: favoritedMovies,
                                        cacheManager: mockedCacheManager)
        //then
        XCTAssertTrue(movieViewModel.isFavorited())
    }
}
