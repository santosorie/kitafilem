//
//  MockedMovieFetcher.swift
//  kitaPelemTests
//
//  Created by Harrie Santoso on 10/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation
@testable import kitaPelem

class MockedMovieFetcher: MovieFetcherInterface {
    var stimulateSuccess = false
    var mockedMovieResult: [Movie] = []
    var isGetMovieCalled = false
    var isGetPopularCalled = false
    var isGetTopRatedCalled = false
    var isGetNowPlayingCalled = false
    
    func getMovieList(fRespone: @escaping responseClosure) {
        isGetMovieCalled = true
        handleMockResponse(fRespone)
    }
    
    func getPopular(fRespone: @escaping responseClosure) {
        isGetPopularCalled = true
        handleMockResponse(fRespone)
    }
    
    func getTopRated(fRespone: @escaping responseClosure) {
        isGetTopRatedCalled = true
        handleMockResponse(fRespone)
    }
    
    func getNowPlaying(fRespone: @escaping responseClosure) {
        isGetNowPlayingCalled = true
        handleMockResponse(fRespone)
    }
    
    private func handleMockResponse(_ response: @escaping responseClosure) {
        if stimulateSuccess {
            response(.success(mockedMovieResult))
        } else {
            response(.error)
        }
    }
}
