//
//  Randomizer.swift
//  kitaPelemTests
//
//  Created by Harrie Santoso on 10/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation
@testable import kitaPelem

struct Randomizer {
    let words: [String] = ["cabe", "paprika", "ayam", "cintah"]
    
    func getBool() -> Bool {
        return getNumber() % 2 == 0
    }
    
    func getId() -> Int64 {
        return Int64.random(in: 0..<10000)
    }
    
    func getNumber() -> Int {
        return Int.random(in: 0..<10000)
    }
    
    func getWord() -> String {
        let randomIndex = Int.random(in: 0..<words.count - 1)
        return words[randomIndex]
    }
    
    func getMovie(with id: Int64? = nil) -> Movie {
        let movieId: Int64
        if let id = id {
            movieId = id
        } else {
            movieId = getId()
        }
        
        return Movie(id: movieId, title: getWord(), releaseDate: getWord(), overview: getWord(), posterPath: getWord())
    }
    
    func getReview() -> Review {
        return Review(id: "\(getWord())-\(getId())", author: getWord(), content: getWord())
    }
}
