//
//  MockedReviewFetcher.swift
//  kitaPelemTests
//
//  Created by Harrie Santoso on 13/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation
@testable import kitaPelem

class MockedReviewFetcher: ReviewFetcherInterface {
    var stimulateSuccess = false
    var mockedReviewResult: [Review] = []
    
    func getReviewList(movieId: Int64, fResponse: @escaping (ReviewResponse) -> Void) {
        if stimulateSuccess {
            fResponse(.success(mockedReviewResult))
        } else {
            fResponse(.error)
        }
    }
}
