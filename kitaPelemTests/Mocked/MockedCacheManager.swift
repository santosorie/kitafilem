//
//  MockedCacheManager.swift
//  kitaPelemTests
//
//  Created by Harrie Santoso on 12/06/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation
@testable import kitaPelem

class MockedCacheManager: CacheManager {
    var randomizer = Randomizer()
    lazy var savedMovie: Movie = randomizer.getMovie()
    lazy var removedMovie: Movie = randomizer.getMovie()
    var mockedFavoriteMovies: [Movie]?
    
    override init() {
        super.init()
    }
    
    override var favoriteMovies: [Movie]? {
        return mockedFavoriteMovies
    }
    
    override func saveMovie(_ movie: Movie) {
        savedMovie = movie
    }
    
    override func removeMovie(_ movie: Movie) {
        removedMovie = movie
    }
}
